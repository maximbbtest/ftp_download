#!/bin/bash
function func_help ()
{
	echo "Скрипт предназначен для загрузки файлов с ftp или sftp. Если на пк не хватает нужного софта, недостающие пакеты установятся автоматически."
	echo "Кроме пакетов, которые нужны для загрузки, устанавливается автодеплоер phploy, который используется для выгрузки проекта на ftp или sftp."
	echo "В каталоге, куда был загружен проект, автоматически создаётся файл phploy.ini, в котором содержаться все настройки для подключения к удалённому серверу."
	echo "После первого коммита, когда все нужные файлы были залиты в репозиторий, обязательно выполнить команду 'phploy --sync', которая создаёт контрольную точку на сервере. "
	echo "Делается это для того, чтобы при первом деплое phploy зпгружал на сервер только изменённые файлы."
        echo ""
        echo "OPTIONS"
        echo -e "\t -s: В этой опции указывается протокол передачи данных  - ftp или sftp. По умолчанию используется ftp"
        echo -e "\t -u: Имя пользователя для подключения"
        echo -e "\t -p: Пароль пользователя"
        echo -e "\t -P: Порт подключения к хосту. Если выбран протокол sftp, по умолчанию порт 22, если ftp - 21"
        echo -e "\t -h: Имя хоста или IP адрс, с которого нужно стянуть сайт"
        echo -e "\t -d: Каталог на сервере, откуда нужно загружайть файлы"
        echo -e "\t -o: Локальный каталог, куда нужно загружать файлы. По умолчанию файлы загружаются в текущий каталог"    
        echo -e "\t -i: Путь к ключу ssh "
        echo -e "\t -?: Справка"
        echo ""
        echo "Примеры."
        echo -e "\tПодключение по sftp с ssh ключём:"
        echo -e "\t upl -i /home/user/.ssh/test.pem -s sftp -u username -h example.com -d /path/to/site -o /local/path/to/files"
        echo -e "\tПодключение по sftp с паролем:"
        echo -e "\t upl -i /home/user/.ssh/test.pem -s sftp -u username -h example.com -d /path/to/site -o /local/path/to/files"
        echo -e "\tПодключение по ftp:"
        echo -e "\t upl -u username -p your_secure_pass -h example.com -d /path/to/site -o /local/path/to/files"
        exit 0;
}
#REPO=
REMOTEFOLDER=/
LOCALFOLDER=./
SCHEME=ftp
PHPLOYCONFIG=phploy.ini

if [ $# -eq 0 ];
then
    func_help
fi

while getopts "s:u:p:P:h:d:o:i:?" Option
do
    case $Option in
    s)	SCHEME=$OPTARG;
	;;
    u)	LOGIN=$OPTARG;
	;;
    p)	PASSWD=$OPTARG;
	;;
    P)	PORT=$OPTARG;
	;;
    h)	HOST=$OPTARG;
	;;
    d)	REMOTEFOLDER=$OPTARG;
	;;
    o)	LOCALFOLDER=$OPTARG;
	;;
    i)	SSHKEY=$OPTARG;
	;;
    ?)	func_help
	;;
    esac
done
WORKDIR=$(cd $LOCALFOLDER && pwd)

echo $LOCALFOLDER
echo $WORKDIR

if ! [ -d "$WORKDIR" ];
    then
	mkdir $WORKDIR
    else

        echo "folder $WORKDIR alredy exists"
        echo -n "Do you really want to download all files in a $WORKDIR (y/n) "
        read item
        case "$item" in
            y|Y) echo "Continue download to $WORKDIR"
                 ;;
            n|N) echo "exiting"
                 exit 0
                 ;;
        esac
    fi

if [ -z "$PORT" ] && [[ $SCHEME == "sftp" ]];
    then
        PORT="22"
    elif [ -z "$PORT" ] && [[ $SCHEME == "ftp" ]];
    then
        PORT="21"
    fi
    
##Check installs
if [[ $(uname -s) == "Darwin" ]];
    then
    if ! [ -f "$(which xcode-select)" ];
        then
            echo "installing xcode"
            echo "after xcode being installed rerun script"
            xcode-select --install
            exit
        fi
        if ! [ -f "$(which brew)" ];
        then
            echo "installing brew"
            ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
        fi
        if ! [ -f "$(which wget)" ];
        then
            echo "installing wget"
            sudo chown -R $USER /usr/local
            brew install wget
        fi
        if ! [ -f "$(which lftp)" ];
        then
            echo "installing lftp"
            sudo chown -R $USER /usr/local
            brew install lftp
        fi
	if ! [ -f "$(which phploy)" ];
        then
            echo "phploy not installed"
	    echo "installing..."
	    sudo wget https://github.com/banago/PHPloy/raw/master/bin/phploy.phar -O /usr/local/bin/phploy
	    sudo chmod 777 /usr/local/bin/phploy
	fi	
    else
        if  [ -z "$(dpkg -s lftp | grep Status)" ]
        then
            echo "Installing lftp"
            sudo apt-get install lftp
        fi
        if  [ -z "$(dpkg -s composer | grep Status)" ]
        then
            echo "Installing composer"
            sudo apt-get install composer
        fi
        if ! [ -f /usr/local/bin/phploy ]
        then
            echo "phploy not installed"
            echo "installing..."
            sudo wget https://github.com/banago/PHPloy/raw/master/bin/phploy.phar -O /usr/local/bin/phploy
            sudo chmod 777 /usr/local/bin/phploy
        fi
fi

#Create phploy.ini config file
echo "[production]" >> $WORKDIR/$PHPLOYCONFIG
echo -e "\tscheme = $SCHEME" >> $WORKDIR/$PHPLOYCONFIG
echo -e "\tuser = $LOGIN" >> $WORKDIR/$PHPLOYCONFIG
echo -e "\tpass = $PASSWD" >> $WORKDIR/$PHPLOYCONFIG
echo -e "\thost = $HOST" >> $WORKDIR/$PHPLOYCONFIG
echo -e "\tpath = $REMOTEFOLDER" >> $WORKDIR/$PHPLOYCONFIG
echo -e "\tport = $PORT" >> $WORKDIR/$PHPLOYCONFIG
case $SSHKEY in
    *)  
    echo -e "\tprivkey = $SSHKEY" >> $WORKDIR/$PHPLOYCONFIG
    ;;
esac

if [[ $SCHEME == "sftp" ]]
    then
        case $SSHKEY in
        "") rsync -auvz -e "ssh -p $PORT" $LOGIN@$HOST:$REMOTEFOLDER/* $WORKDIR --progress
            ;;
        *)  rsync -auvz -e "ssh -p $PORT -i $SSHKEY" $LOGIN@$HOST:$REMOTEFOLDER/* $WORKDIR --progress
            ;;
        esac
    else
        lftp -e "mirror --verbose -c $REMOTEFOLDER $WORKDIR; bye;" ftp://$LOGIN:$PASSWD@$HOST
    fi
du -s $WORKDIR/* | sort -nr | cut -f 2- | while read a; do du -hs $a; done